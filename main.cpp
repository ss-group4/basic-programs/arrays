#include <iostream>

using namespace std;

class adt {
public:
    int* a;
    int size, last;

    explicit adt(int len) {
        last = -1;
        size = len;
        a = new int[size];
    }

    void insert(int key, int index) {
        if (size == last + 1)
            return;

        if (index == -1)
            index = last + 1;

        // lets insert element at index

        for (int i = last; i >= index; --i)
            a[i + 1] = a[i];
        a[index] = key;
        last++;
    }

    [[nodiscard]] bool is_sorted(bool rev = false) const {
        for (int i = 0; i < last; ++i) {
            if ((!rev && a[i] > a[i + 1]) || (rev && a[i] < a[i + 1])) {
                cout << "Unsorted array" << endl;
                return false;
            }
        }
        cout << "Sorted array" << endl;
        return true;
    }

    void sort(bool rev = false) {
        if (is_sorted(rev))
            return;

        for (int i = 0; i < last; ++i) {
            for (int j = i + 1; j <= last; ++j) {
                if ((!rev && a[i] >= a[j]) || (rev && a[i] <= a[j])) {
                    swap(a[i], a[j]);
                }
            }
        }
    }

    void remove_at(int index) {
        if (index + 1 > size || index < 0 || index > last)
            return;
        for (int i = index; i < last; i++)
            a[i] = a[i + 1];
        this->last--;
    }

    int operator[](int index) const {
        if (abs(index) >= size) {
            cout << "Index out of bounds";
            return -1;
        }
        if (index < 0)
            index = last + 1 + index;

        return a[index];
    }

    void remove_duplicates() {
        if (!is_sorted()) {
            for (int i = 0; i < last; ++i) {
                for (int j = i; j <= last; ++j) {
                    if (a[i] == a[j]) {
                        remove_at(j);
                        break; // After removal, break the inner loop
                    }
                }
            }
        } else {
            int i = 0;
            for (int j = 1; j <= last; ++j) {
                if (a[i] != a[j]) {
                    a[++i] = a[j];
                }
            }
            last = i;
        }
    }
    int find(int key){
        if(last == -1)
            return -1;
        for (int i = 0; i <=last; i++) {
            if (a[i] == key)
                return i;
        }
        return -1;
    }

    void reverse() {
        int i = 0, j = last;
        while (i < j) {
            swap(a[i], a[j]);
            i++, j--;
        }
    }

    void print() const {
        cout << "Array Contents: ";
        for (int x = 0; x <= last; ++x)
            cout << a[x] << " ";
        cout << endl;
    }

    ~adt() {
        delete[] a;
    }
};

adt merge_sorted(const adt& f, const adt& g) {
    int s = f.last + 1 + g.last + 1;
    adt h(s);
    int j = 0, k = 0;
    if(not f.is_sorted() or not g.is_sorted())
        return h;
    while (j <= f.last and k <= g.last) {
        if (f.a[j] <= g.a[k] ){
            if( h.find(f.a[j]) == -1)
                h.insert(f.a[j], h.last+1 );
            ++j;
        }
        else if (f.a[j] > g.a[k] ){
            if( h.find(g.a[k]) == -1)
                h.insert(g.a[k], h.last+1);
            ++k;
        }
        j++;
    }

    // Copy any remaining elements from f, if any
    while (j <= f.last) {
        if( h.find(f.a[j]) == -1)
            h.insert(f.a[j], -1);
        ++j;
    }

    // Copy any remaining elements from g, if any
    while (k <= g.last) {
        if( h.find(g.a[k]) == -1)
            h.insert(g.a[k], h.last+1);
        ++k;
    }

    return h;
}

int main() {
    adt g(10), h(10);
    g.insert(10, 0);
    g.insert(20, 0);
    g.insert(20, 0);
    g.insert(20, 0);
    g.insert(30, 0);
    g.insert(240, 1);

    for (int i = 10; i < 20; ++i)
        h.insert(i, 0);

    h.reverse();
    g.print();
    g.remove_at(1);
    g.sort();
    g.remove_duplicates();
    g.print();
    cout << g[-1] << "\nReversed array: ";

    //g.reverse();
    g.print();
    h.print();
    adt f = merge_sorted(g, h);
    f.print();

    return 0;
}
